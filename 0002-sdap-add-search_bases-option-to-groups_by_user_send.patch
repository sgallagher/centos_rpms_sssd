From a7621a5b464af7a3c8409dcbde038b35fee2c895 Mon Sep 17 00:00:00 2001
From: Sumit Bose <sbose@redhat.com>
Date: Tue, 23 Jan 2024 13:47:53 +0100
Subject: [PATCH 2/3] sdap: add search_bases option to groups_by_user_send()
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit

AD handles users and computer objects very similar and so does SSSD's
GPO code when lookup up the host's group-memberships. But users and
computers might be stored in different sub-tree of the AD LDAP tree and
if a dedicated user search base is given with the ldap_user_search_base
option in sssd.conf the host object might be in a different sub-tree. To
make sure the host can still be found this patch uses the base DN of
the LDAP tree when searching for hosts in the GPO code.

Resolves: https://github.com/SSSD/sssd/issues/5708

Reviewed-by: Alejandro López <allopez@redhat.com>
Reviewed-by: Tomáš Halman <thalman@redhat.com>
(cherry picked from commit 29a77c6e79020d7e8cb474b4d3b394d390eba196)
---
 src/providers/ad/ad_gpo.c                  | 10 ++++++++++
 src/providers/ldap/ldap_common.h           |  1 +
 src/providers/ldap/ldap_id.c               |  6 +++++-
 src/providers/ldap/sdap_async.h            |  1 +
 src/providers/ldap/sdap_async_initgroups.c |  4 +++-
 5 files changed, 20 insertions(+), 2 deletions(-)

diff --git a/src/providers/ad/ad_gpo.c b/src/providers/ad/ad_gpo.c
index 94959c36b..b0ee3e616 100644
--- a/src/providers/ad/ad_gpo.c
+++ b/src/providers/ad/ad_gpo.c
@@ -2091,6 +2091,7 @@ ad_gpo_connect_done(struct tevent_req *subreq)
     char *server_uri;
     LDAPURLDesc *lud;
     struct sdap_domain *sdom;
+    struct sdap_search_base **search_bases;
 
     req = tevent_req_callback_data(subreq, struct tevent_req);
     state = tevent_req_data(req, struct ad_gpo_access_state);
@@ -2184,9 +2185,18 @@ ad_gpo_connect_done(struct tevent_req *subreq)
         goto done;
     }
 
+    ret = common_parse_search_base(state, sdom->basedn, state->ldb_ctx,
+                                   "AD_HOSTS", NULL, &search_bases);
+    if (ret != EOK) {
+        DEBUG(SSSDBG_OP_FAILURE,
+              "Failed to create dedicated search base for host lookups, "
+              "trying with user search base.");
+    }
+
     subreq = groups_by_user_send(state, state->ev,
                                  state->access_ctx->ad_id_ctx->sdap_id_ctx,
                                  sdom, state->conn,
+                                 search_bases,
                                  state->host_fqdn,
                                  BE_FILTER_NAME,
                                  NULL,
diff --git a/src/providers/ldap/ldap_common.h b/src/providers/ldap/ldap_common.h
index 7159d6356..2c984ef50 100644
--- a/src/providers/ldap/ldap_common.h
+++ b/src/providers/ldap/ldap_common.h
@@ -304,6 +304,7 @@ struct tevent_req *groups_by_user_send(TALLOC_CTX *memctx,
                                        struct sdap_id_ctx *ctx,
                                        struct sdap_domain *sdom,
                                        struct sdap_id_conn_ctx *conn,
+                                       struct sdap_search_base **search_bases,
                                        const char *filter_value,
                                        int filter_type,
                                        const char *extra_value,
diff --git a/src/providers/ldap/ldap_id.c b/src/providers/ldap/ldap_id.c
index da54816bd..b3ea2333f 100644
--- a/src/providers/ldap/ldap_id.c
+++ b/src/providers/ldap/ldap_id.c
@@ -1139,6 +1139,7 @@ struct groups_by_user_state {
     struct sdap_id_op *op;
     struct sysdb_ctx *sysdb;
     struct sss_domain_info *domain;
+    struct sdap_search_base **search_bases;
 
     const char *filter_value;
     int filter_type;
@@ -1160,6 +1161,7 @@ struct tevent_req *groups_by_user_send(TALLOC_CTX *memctx,
                                        struct sdap_id_ctx *ctx,
                                        struct sdap_domain *sdom,
                                        struct sdap_id_conn_ctx *conn,
+                                       struct sdap_search_base **search_bases,
                                        const char *filter_value,
                                        int filter_type,
                                        const char *extra_value,
@@ -1192,6 +1194,7 @@ struct tevent_req *groups_by_user_send(TALLOC_CTX *memctx,
     state->extra_value = extra_value;
     state->domain = sdom->dom;
     state->sysdb = sdom->dom->sysdb;
+    state->search_bases = search_bases;
 
     if (state->domain->type == DOM_TYPE_APPLICATION || set_non_posix) {
         state->non_posix = true;
@@ -1254,6 +1257,7 @@ static void groups_by_user_connect_done(struct tevent_req *subreq)
                                   sdap_id_op_handle(state->op),
                                   state->ctx,
                                   state->conn,
+                                  state->search_bases,
                                   state->filter_value,
                                   state->filter_type,
                                   state->extra_value,
@@ -1449,7 +1453,7 @@ sdap_handle_acct_req_send(TALLOC_CTX *mem_ctx,
         }
 
         subreq = groups_by_user_send(state, be_ctx->ev, id_ctx,
-                                     sdom, conn,
+                                     sdom, conn, NULL,
                                      ar->filter_value,
                                      ar->filter_type,
                                      ar->extra_value,
diff --git a/src/providers/ldap/sdap_async.h b/src/providers/ldap/sdap_async.h
index 5458d21f1..89245f41f 100644
--- a/src/providers/ldap/sdap_async.h
+++ b/src/providers/ldap/sdap_async.h
@@ -158,6 +158,7 @@ struct tevent_req *sdap_get_initgr_send(TALLOC_CTX *memctx,
                                         struct sdap_handle *sh,
                                         struct sdap_id_ctx *id_ctx,
                                         struct sdap_id_conn_ctx *conn,
+                                        struct sdap_search_base **search_bases,
                                         const char *name,
                                         int filter_type,
                                         const char *extra_value,
diff --git a/src/providers/ldap/sdap_async_initgroups.c b/src/providers/ldap/sdap_async_initgroups.c
index 97be594a3..fb3d8fe24 100644
--- a/src/providers/ldap/sdap_async_initgroups.c
+++ b/src/providers/ldap/sdap_async_initgroups.c
@@ -2732,6 +2732,7 @@ struct tevent_req *sdap_get_initgr_send(TALLOC_CTX *memctx,
                                         struct sdap_handle *sh,
                                         struct sdap_id_ctx *id_ctx,
                                         struct sdap_id_conn_ctx *conn,
+                                        struct sdap_search_base **search_bases,
                                         const char *filter_value,
                                         int filter_type,
                                         const char *extra_value,
@@ -2764,7 +2765,8 @@ struct tevent_req *sdap_get_initgr_send(TALLOC_CTX *memctx,
     state->orig_user = NULL;
     state->timeout = dp_opt_get_int(state->opts->basic, SDAP_SEARCH_TIMEOUT);
     state->user_base_iter = 0;
-    state->user_search_bases = sdom->user_search_bases;
+    state->user_search_bases = (search_bases == NULL) ? sdom->user_search_bases
+                                                      : search_bases;
     if (!state->user_search_bases) {
         DEBUG(SSSDBG_CRIT_FAILURE,
               "Initgroups lookup request without a user search base\n");
-- 
2.41.0

